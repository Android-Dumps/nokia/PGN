#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_PGN.mk

COMMON_LUNCH_CHOICES := \
    lineage_PGN-user \
    lineage_PGN-userdebug \
    lineage_PGN-eng
