#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from PGN device
$(call inherit-product, device/hmd/PGN/device.mk)

PRODUCT_DEVICE := PGN
PRODUCT_NAME := lineage_PGN
PRODUCT_BRAND := Nokia
PRODUCT_MODEL := Nokia T10
PRODUCT_MANUFACTURER := hmd

PRODUCT_GMS_CLIENTID_BASE := android-hmd

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="ums9230_4h10_Natv-user 13 TP1A.220624.014 18606 release-keys"

BUILD_FINGERPRINT := Nokia/Penguin_00WW/PGN:13/TP1A.220624.014/00WW_2_310:user/release-keys
