#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Enable updating of APEXes
$(call inherit-product, $(SRC_TARGET_DIR)/product/updatable_apex.mk)

# Include GSI keys
$(call inherit-product, $(SRC_TARGET_DIR)/product/gsi_keys.mk)

# A/B
$(call inherit-product, $(SRC_TARGET_DIR)/product/virtual_ab_ota.mk)

PRODUCT_PACKAGES += \
    android.hardware.boot@1.2-impl \
    android.hardware.boot@1.2-impl.recovery \
    android.hardware.boot@1.2-service

PRODUCT_PACKAGES += \
    update_engine \
    update_engine_sideload \
    update_verifier

PRODUCT_PACKAGES += \
    checkpoint_gc \
    otapreopt_script

# fastbootd
PRODUCT_PACKAGES += \
    android.hardware.fastboot@1.1-impl-mock \
    fastbootd

# Health
PRODUCT_PACKAGES += \
    android.hardware.health@2.1-impl \
    android.hardware.health@2.1-service

# Overlays
PRODUCT_ENFORCE_RRO_TARGETS := *

# Partitions
PRODUCT_BUILD_SUPER_PARTITION := false
PRODUCT_USE_DYNAMIC_PARTITIONS := true

# Product characteristics
PRODUCT_CHARACTERISTICS := tablet

# Rootdir
PRODUCT_PACKAGES += \
    para.sh \
    loading.sh \
    log_to_csv.sh \
    total.sh \
    init.insmod.sh \

PRODUCT_PACKAGES += \
    fstab.enableswap \
    init.ums9230_haps.rc \
    init.ums9230_haps.usb.rc \
    init.ums9230_4h10.usb.rc \
    init.ums9230_zebu.usb.rc \
    init.storage.rc \
    init.ram.native.rc \
    init.ums9230_6h10.usb.rc \
    init.ram.gms.rc \
    init.ram.rc \
    init.ums9230_7h10.rc \
    init.ums9230_4h10_go.usb.rc \
    init.ums9230_4h10.rc \
    init.ums9230_zebu.rc \
    init.ums9230_4h10_go.rc \
    init.ums9230_6h10.rc \
    init.cali.rc \
    init.ums9230_1h10_go.rc \
    init.ums9230_1h10.usb.rc \
    init.ums9230_1h10_go.usb.rc \
    init.ums9230_1h10.rc \
    init.ums9230_7h10.usb.rc \

PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/rootdir/etc/fstab.enableswap:$(TARGET_VENDOR_RAMDISK_OUT)/first_stage_ramdisk/fstab.enableswap

# Shipping API level
PRODUCT_SHIPPING_API_LEVEL := 31

# Soong namespaces
PRODUCT_SOONG_NAMESPACES += \
    $(LOCAL_PATH)

# Inherit the proprietary files
$(call inherit-product, vendor/hmd/PGN/PGN-vendor.mk)
